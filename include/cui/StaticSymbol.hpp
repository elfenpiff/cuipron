#pragma once

#include "cui/BaseWidget.hpp"
#include "cui/widgetProperties_t.hpp"

namespace cui
{
class StaticSymbol : public BaseWidget
{
  public:
    StaticSymbol( cp::Renderer* const       renderer,
                  const widgetProperties_t& properties ) noexcept;

    void
    SetContent( const std::string& value ) noexcept;
    void
    Draw() noexcept override;

  private:
    std::string content;
    modList_t   style{ cp::Background_Default, cp::Foreground_Default };
};
} // namespace cui
