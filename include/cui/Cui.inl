
namespace cui
{
template < typename T, typename... Targs >
inline std::shared_ptr< T >
Cui::Create( const cui::widgetProperties_t properties,
             Targs&&... args ) noexcept
{
    T* newWidget =
        new T( &this->renderer, properties, std::forward< Targs >( args )... );
    this->widgets.emplace_back( newWidget );
    return std::shared_ptr< T >( newWidget, [this]( T* widget ) {
        this->Remove( widget );
        delete widget;
    } );
}
} // namespace cui
