#pragma once

#include <cstdint>

namespace cp
{
struct vec2
{
    uint16_t x;
    uint16_t y;

    bool
    operator==( const vec2& rhs ) const noexcept
    {
        return this->x == rhs.x && this->y == rhs.y;
    }

    bool
    operator!=( const vec2& rhs ) const noexcept
    {
        return !( *this == rhs );
    }
};
} // namespace cp
