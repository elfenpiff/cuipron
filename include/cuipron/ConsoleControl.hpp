#pragma once

#include "cuipron/vec2.hpp"

namespace cp
{
class ConsoleControl
{
  public:
    ConsoleControl() noexcept;

    vec2
    GetTerminalDimensions() const noexcept;
    void
    ClearTerminal() const noexcept;
    void
    SetCursorPosition( const vec2& position ) const noexcept;
    vec2
    GetCursorPosition() const noexcept;

    bool
    HasOutputToTerminal() const noexcept;
    void
    HideCursor() const noexcept;
    void
    ShowCursor() const noexcept;

    void
    Refresh() noexcept;

  private:
    void
    RefreshDimensions() noexcept;

  private:
    vec2 terminalDimensions;
};
} // namespace cp
