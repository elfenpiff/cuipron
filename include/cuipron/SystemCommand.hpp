#pragma once

#include <future>
#include <mutex>
#include <string>
#include <vector>

namespace cp
{
class SystemCommand
{
  public:
    SystemCommand( const std::string &command ) noexcept;
    /// @brief Can be executed just once until the call is finished. This call
    ///         is blocking and returns true on success, otherwise false.
    bool
    Execute() noexcept;

    /// @brief Can be executed just once until the call is finished. This call
    ///         is blocking and returns the output on success.
    std::vector< std::string >
    ExecuteAndGet() noexcept;

    /// @brief This method can be called threadsafe from multiple threads.
    ///         During a long Execute() you can get the intermediate output
    ///         while calling this function continuously.
    ///        New output of the call is added to this vector line by line.
    std::vector< std::string >
    GetOutput() const noexcept;

    /// @brief Calls Execute asynchronously.
    void
    AsyncExecute() noexcept;
    bool
    IsAsyncExecuteFinished() const noexcept;
    bool
    IsAsyncExecuteSuccessful() const noexcept;
    int
    GetExitCode() const noexcept;


  private:
    std::string                command;
    mutable std::mutex         outputMutex;
    std::vector< std::string > output;
    int                        exitCode{ 0 };

    mutable std::future< bool > asyncExecute;
    mutable bool                isAsyncExecuteSuccessful{ false };
};
} // namespace cp
