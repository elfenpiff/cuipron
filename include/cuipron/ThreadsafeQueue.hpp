#pragma once

#include <mutex>
#include <optional>
#include <queue>

namespace cp
{
template < typename T >
class ThreadsafeQueue
{
  public:
    void
    push( const T& value ) noexcept;
    std::optional< T >
    pop() noexcept;
    bool
    empty() const noexcept;

  private:
    std::queue< T >    queue;
    mutable std::mutex mutex;
};
} // namespace cp

#include "cuipron/ThreadsafeQueue.inl"
