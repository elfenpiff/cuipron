#pragma once

#include <signal.h>

void
signalHandler( int sig );

auto setSignal = [] { return signal( SIGINT, signalHandler ); }();
