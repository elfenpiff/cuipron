#include "cui/KeyValuePair.hpp"

namespace cui
{
KeyValuePair::KeyValuePair( cp::Renderer* const       renderer,
                            const widgetProperties_t& properties ) noexcept
    : BaseWidget( renderer, properties )
{
}

void
KeyValuePair::Draw() noexcept
{
    BaseWidget::Draw();

    std::string title, value, unit;
    if ( this->titleCallback ) title = this->titleCallback();
    if ( this->valueCallback ) value = this->valueCallback();
    if ( this->unitCallback ) unit = this->unitCallback();

    this->renderer->ResetContentModifier( this->position.x, this->position.y,
                                          this->titleStyle );
    this->renderer->SetContentValue( this->position.x, this->position.y,
                                     title + " " );

    this->renderer->ResetContentModifier( this->position.x + title.size() + 1,
                                          this->position.y, this->valueStyle );
    this->renderer->SetContentValue( this->position.x + title.size() + 1,
                                     this->position.y, value + " " );

    this->renderer->ResetContentModifier( this->position.x + title.size() +
                                              value.size() + 2,
                                          this->position.y, this->unitStyle );
    this->renderer->SetContentValue( this->position.x + title.size() +
                                         value.size() + 2,
                                     this->position.y, unit + " " );
}

void
KeyValuePair::SetTitle( const set_t< std::string >& title ) noexcept
{
    this->titleCallback = title;
}

void
KeyValuePair::SetValue( const set_t< std::string >& value ) noexcept
{
    this->valueCallback = value;
}

void
KeyValuePair::SetUnit( const set_t< std::string >& unit ) noexcept
{
    this->unitCallback = unit;
}


} // namespace cui
