#include "cui/BaseWidget.hpp"

#include "cuipron/Renderer.hpp"

namespace cui
{
BaseWidget::BaseWidget( cp::Renderer* const       renderer,
                        const widgetProperties_t& properties ) noexcept
    : renderer( renderer ), size( properties.size ),
      position( properties.position )
{
}

cp::vec2
BaseWidget::GetSize() const noexcept
{
    return this->size;
}

void
BaseWidget::SetSize( const cp::vec2& value ) noexcept
{
    this->setter.push( [this, value] { this->size = value; } );
}

cp::vec2
BaseWidget::GetPosition() const noexcept
{
    return this->position;
}

void
BaseWidget::SetPosition( const cp::vec2& value ) noexcept
{
    this->setter.push( [this, value] { this->position = value; } );
}

void
BaseWidget::Draw() noexcept
{
    while ( !this->setter.empty() )
        ( *this->setter.pop() )();
}


} // namespace cui

