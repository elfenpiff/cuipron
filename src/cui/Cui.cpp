#include "cui/Cui.hpp"

#include "cuipron/ConsoleControl.hpp"
#include "cuipron/Renderer.hpp"

namespace cui
{
Cui::Cui( const uint16_t lineSpacing ) noexcept
    : startupCursorPosition( [&] {
          AddLineSpacing( lineSpacing );
          return cp::ConsoleControl().GetCursorPosition();
      }() ),
      renderer( [this]() { this->Draw(); }, this->fps )
{
}

Cui::~Cui()
{
    this->widgets.clear();
}

void
Cui::AddLineSpacing( const uint16_t n ) const noexcept
{
    for ( uint16_t k = 0; k < n; ++k )
        std::cout << std::endl;
}

cp::vec2
Cui::GetStartupCursorPosition() const noexcept
{
    return this->startupCursorPosition;
}

void
Cui::Remove( BaseWidget* const widget ) noexcept
{
    auto iter = std::find( this->widgets.begin(), this->widgets.end(), widget );
    if ( iter == this->widgets.end() ) return;

    this->widgets.erase( iter );
}

void
Cui::Draw() const noexcept
{
    for ( auto& w : this->widgets )
        w->Draw();
}

void
Cui::WaitForExitSignal() const noexcept
{
    std::unique_lock< std::mutex > lock( this->signalMutex );
    this->signalCondition.wait( lock, [&] { return this->hasExitSignal; } );
}

void
Cui::SendExitSignal() const noexcept
{
    std::unique_lock< std::mutex > lock( this->signalMutex );
    this->hasExitSignal = true;
    this->signalCondition.notify_all();
}


} // namespace cui
