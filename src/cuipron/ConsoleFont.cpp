#include "cuipron/ConsoleFont.hpp"

namespace cp
{

ConsoleFont::ConsoleFont( const Modifier modifier ) noexcept
    : modifier( static_cast< int >( modifier ) )
{
}

std::ostream&
operator<<( std::ostream& out, const ConsoleFont& property ) noexcept
{
    if ( property.modifier == -1 ) return out;

    out << "\033[" << property.modifier << "m";
    return out;
}
} // namespace cp
