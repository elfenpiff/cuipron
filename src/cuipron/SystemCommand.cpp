#include "cuipron/SystemCommand.hpp"

#include <array>
#include <cstdio>
#include <iostream>

namespace cp
{

SystemCommand::SystemCommand( const std::string &command ) noexcept
    : command( command )
{
}

bool
SystemCommand::Execute() noexcept
{
    {
        std::lock_guard< std::mutex > lock( this->outputMutex );
        this->output.clear();
    }

#ifndef WIN32
    FILE *pipe = popen( this->command.c_str(), "r" );
#else
    FILE *pipe     = _popen( this->command.c_str(), "r" );
#endif

    if ( pipe == nullptr )
    {
        std::cerr << "unable to execute system command : " << command
                  << std::endl;
        return false;
    }

    std::array< char, 1024 > buffer;
    while ( fgets( buffer.data(), buffer.size(), pipe ) != nullptr )
    {
        std::string temp( buffer.data() );
        if ( temp.size() > 1 ) temp.resize( temp.size() - 1 );
        {
            std::lock_guard< std::mutex > lock( this->outputMutex );
            output.push_back( temp );
        }
    }

#ifndef WIN32
    this->exitCode = pclose( pipe ) / 256;
#else
    this->exitCode = _pclose( pipe ) / 256;
#endif
    return true;
}

int
SystemCommand::GetExitCode() const noexcept
{
    return this->exitCode;
}

std::vector< std::string >
SystemCommand::ExecuteAndGet() noexcept
{
    this->Execute();
    return this->GetOutput();
}

std::vector< std::string >
SystemCommand::GetOutput() const noexcept
{
    std::lock_guard< std::mutex > lock( this->outputMutex );
    return this->output;
}

void
SystemCommand::AsyncExecute() noexcept
{
    {
        std::lock_guard< std::mutex > lock( this->outputMutex );
        this->output.clear();
    }
    this->isAsyncExecuteSuccessful = false;
    this->asyncExecute =
        std::async( std::launch::async, [this] { return this->Execute(); } );
}

bool
SystemCommand::IsAsyncExecuteFinished() const noexcept
{
    return ( !this->asyncExecute.valid() ||
             ( this->asyncExecute.valid() &&
               this->asyncExecute.wait_for( std::chrono::seconds( 0 ) ) ==
                   std::future_status::ready ) );
}

bool
SystemCommand::IsAsyncExecuteSuccessful() const noexcept
{
    if ( this->asyncExecute.valid() )
        this->isAsyncExecuteSuccessful = this->asyncExecute.get();
    return this->isAsyncExecuteSuccessful;
}


} // namespace cp
