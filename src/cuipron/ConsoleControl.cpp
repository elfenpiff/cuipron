#include "cuipron/ConsoleControl.hpp"

#include <cstdio>

#if _WIN32
#include <io.h>
#else
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

#endif

namespace cp
{
ConsoleControl::ConsoleControl() noexcept
{
    this->Refresh();
}

bool
ConsoleControl::HasOutputToTerminal() const noexcept
{
#if _WIN32
    return _isatty( _fileno( stdout ) );
#else
    return isatty( fileno( stdout ) );
#endif
}

void
ConsoleControl::HideCursor() const noexcept
{
    printf( "\033[?25l" );
}

void
ConsoleControl::ShowCursor() const noexcept
{
    printf( "\033[?25h\n" );
}

void
ConsoleControl::ClearTerminal() const noexcept
{
    printf( "\033[H\033[J" );
}

void
ConsoleControl::SetCursorPosition( const vec2& position ) const noexcept
{
    printf( "\033[%u;%uH", static_cast< unsigned int >( position.y ),
            static_cast< unsigned int >( position.x ) );
}

vec2
ConsoleControl::GetCursorPosition() const noexcept
{
#ifdef _WIN32
    return { 1u, 1u };
#else
    if ( !this->HasOutputToTerminal() ) return { 0u, 0u };

    constexpr char command[]     = "echo -e '\033[6n'";
    constexpr int  commandLength = 10;
    struct termios previousState, rawState;
    tcgetattr( 0, &previousState );
    cfmakeraw( &rawState );
    tcsetattr( 0, TCSANOW, &rawState );

    write( 1, command, sizeof( command ) );

    char buffer[8];
    read( 0, buffer, sizeof( buffer ) );

    int x, y;
    sscanf( buffer, "\033[%d;%dR", &y, &x );
    x -= commandLength;
    y -= 1;

    tcsetattr( 0, TCSANOW, &previousState );
    printf( "\033[2K" );
    return { static_cast< uint16_t >( x ), static_cast< uint16_t >( y ) };
#endif
}

vec2
ConsoleControl::GetTerminalDimensions() const noexcept
{
    return this->terminalDimensions;
}

void
ConsoleControl::RefreshDimensions() noexcept
{
#ifndef _WIN32
    struct winsize size;
    ioctl( 0, TIOCGWINSZ, &size );
    this->terminalDimensions = vec2{ size.ws_col, size.ws_row };
#else
    this->terminalDimensions = vec2{ 40, 20 };
#endif
}

void
ConsoleControl::Refresh() noexcept
{
    this->RefreshDimensions();
}

} // namespace cp
